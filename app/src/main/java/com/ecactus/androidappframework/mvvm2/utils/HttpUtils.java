package com.ecactus.androidappframework.mvvm2.utils;

import android.os.Handler;
import android.os.Looper;

import java.util.Map;
import java.util.Random;

// http工具
public class HttpUtils {
    private static Handler sHandler = new Handler(Looper.getMainLooper());

    public interface ResponseCallback {
        void onResponseSuccessed(String json);
        void onResponseFailed(int reason);
    }

    // 发起http请求，这里使用模拟的数据，并有一定机率请求失败
    public static void request(Map params, final ResponseCallback callback) {
        sHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                int value = new Random(System.currentTimeMillis()).nextInt(5);
                if(callback != null) {
                    if(value == 2) {
                        callback.onResponseFailed(1);
                    } else {
                        callback.onResponseSuccessed("{\"name\": \"纯爷们\", \"age\": 20}");
                    }
                }
            }
        }, 500);
    }
}
