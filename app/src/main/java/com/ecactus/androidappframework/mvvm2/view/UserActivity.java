package com.ecactus.androidappframework.mvvm2.view;

import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.ViewModelProvider;

import com.ecactus.androidappframework.R;
import com.ecactus.androidappframework.databinding.ActivityUserBinding;
import com.ecactus.androidappframework.mvvm2.viewModel.UserViewModel;

public class UserActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ActivityUserBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_user);
        // 获取ViewModel
        ViewModelProvider.AndroidViewModelFactory instance = ViewModelProvider.AndroidViewModelFactory
                        .getInstance(getApplication());
        UserViewModel userViewModel = new ViewModelProvider(this, instance).get(UserViewModel.class);
        userViewModel.observe(this,binding);
    }
}