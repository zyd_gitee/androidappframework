package com.ecactus.androidappframework.mvvm2.utils;

import android.database.Cursor;

// 数据库工具
public class DbUtils {
    // 查询数据库记录并返回cursor，这里使用测试代码直接返回null
    public static Cursor query(String sql) {
        return null;
    }

    // 更新数据库记录，这里使用测试代码不进行任何实际处理
    public static void update(String sql) {

    }
}