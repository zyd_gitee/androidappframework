package com.ecactus.androidappframework.mvvm2.viewModel;

import androidx.lifecycle.MutableLiveData;

import com.ecactus.androidappframework.mvvm2.model.User;

public class UserLiveData extends MutableLiveData<User> {
}
