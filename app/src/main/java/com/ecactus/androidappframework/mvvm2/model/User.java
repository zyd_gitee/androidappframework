package com.ecactus.androidappframework.mvvm2.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.ecactus.androidappframework.BR;

public class User extends BaseObservable {
    @Bindable
    public String name;

    @Bindable
    public int age;

    public void setName(String name) {
        this.name = name;
        notifyPropertyChanged(BR.name);
    }

    public void setAge(int age) {
        this.age = age;
        notifyPropertyChanged(BR.age);
    }
}