package com.ecactus.androidappframework.mvp2.presenter;

import android.content.Context;

import com.ecactus.androidappframework.mvp2.contract.LoginContract;
import com.ecactus.androidappframework.mvp2.model.User;


public class LoginPresenter implements LoginContract.Presenter {

    private Context mContext;
    private LoginContract.View mLoginView;
    private User mUser;

    public LoginPresenter(Context mContext, LoginContract.View mLoginView) {
        this.mContext = mContext;
        this.mLoginView = mLoginView;
        mUser = new User();
        mLoginView.setPresenter(this);
    }

    @Override
    public void login() {
        attemptLogin();
    }

    @Override
    public void reset() {
        mLoginView.resetEditView();
    }

    @Override
    public void start() {

    }

    private void attemptLogin() {
        String userName = mLoginView.getUserName();
        String passWord = mLoginView.getPassWord();

        try {
//            模拟网络耗时操作
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (userName.equals("123") && passWord.equals("456")) {
            mLoginView.LoginSuccess();
        } else {
            mLoginView.LoginFail("密码错误");
        }
    }

}
