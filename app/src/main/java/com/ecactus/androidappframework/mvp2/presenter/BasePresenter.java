package com.ecactus.androidappframework.mvp2.presenter;

public interface BasePresenter {
    void start();
}
