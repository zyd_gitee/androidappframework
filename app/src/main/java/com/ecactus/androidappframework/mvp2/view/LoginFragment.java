package com.ecactus.androidappframework.mvp2.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.ecactus.androidappframework.R;
import com.ecactus.androidappframework.mvp2.contract.LoginContract;

public class LoginFragment extends Fragment implements LoginContract.View {

    private LoginContract.Presenter mPresenter;
    private Button mLoginBtn, mResetBtn;
    private TextView mUserNameEdit, mPassEdit;


    public static LoginFragment newInstance(String id) {
        Bundle args = new Bundle();
        args.putString("fragment_id", id);
        LoginFragment loginFragment = new LoginFragment();
        loginFragment.setArguments(args);
        return loginFragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.start();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        initVew(root);
        return root;
    }

    private void initVew(View view) {

        mUserNameEdit = view.findViewById(R.id.editT_username);
        mPassEdit = view.findViewById(R.id.editT_password);
        mLoginBtn = view.findViewById(R.id.btn_login);
        mResetBtn = view.findViewById(R.id.btn_reset);

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.login();
            }
        });

        mResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mPresenter.reset();
            }
        });
    }

    @Override
    public String getUserName() {
        return mUserNameEdit.getText().toString();
    }

    @Override
    public String getPassWord() {
        return mPassEdit.getText().toString();
    }

    @Override
    public void LoginSuccess() {
        Toast.makeText(getContext(), "登陆成功", Toast.LENGTH_SHORT).show();
    }


    @Override
    public void LoginFail(String error) {
        Toast.makeText(getContext(), "登陆失败", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void resetEditView() {
        mUserNameEdit.setText("");
        mPassEdit.setText("");
    }

    @Override
    public void setPresenter(LoginContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mPresenter != null)
            mPresenter = null;
    }

}