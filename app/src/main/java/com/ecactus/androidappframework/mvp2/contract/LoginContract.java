package com.ecactus.androidappframework.mvp2.contract;


import com.ecactus.androidappframework.mvp2.presenter.BasePresenter;
import com.ecactus.androidappframework.mvp2.view.BaseView;

public interface LoginContract {

    interface View extends BaseView<Presenter> {
        String getUserName();

        String getPassWord();

        void LoginSuccess();

        void LoginFail(String error);

        void resetEditView();
    }

    interface Presenter extends BasePresenter {
        void login();

        void reset();
    }
}