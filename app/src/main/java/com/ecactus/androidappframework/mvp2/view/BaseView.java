package com.ecactus.androidappframework.mvp2.view;

public interface BaseView <T>{
    //这里使用范型
    void setPresenter(T presenter);
}
