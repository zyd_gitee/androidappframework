package com.ecactus.androidappframework.mvp.presenter;

import com.ecactus.androidappframework.mvp.model.User;
import com.ecactus.androidappframework.mvp.view.ILoginView;

public class LoginPresenterImpl implements ILoginPresenter {
    private ILoginView mLoginView;
    private User mUser;

    public LoginPresenterImpl(ILoginView loginView) {
        mLoginView = loginView;
        mUser = new User();
    }

    @Override
    public void login(String username, String password) {
        boolean isValid = mUser.isValid(username, password);
        if (isValid) {
            mLoginView.onShowMessage("login success");
        } else {
            mLoginView.onShowMessage("login failed");
        }
    }

}
