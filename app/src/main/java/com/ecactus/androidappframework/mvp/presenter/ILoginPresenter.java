package com.ecactus.androidappframework.mvp.presenter;

public interface ILoginPresenter {
    void login(String username, String password);
}