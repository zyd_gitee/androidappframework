package com.ecactus.androidappframework.mvp.view;

public interface ILoginView {
    void onShowMessage(String msg);
}