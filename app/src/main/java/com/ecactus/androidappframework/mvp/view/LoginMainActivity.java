package com.ecactus.androidappframework.mvp.view;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.ecactus.androidappframework.R;
import com.ecactus.androidappframework.mvp.presenter.ILoginPresenter;
import com.ecactus.androidappframework.mvp.presenter.LoginPresenterImpl;


public class LoginMainActivity extends AppCompatActivity implements ILoginView, View.OnClickListener {
    ILoginPresenter loginPresenter = new LoginPresenterImpl(this);
    private EditText mUsername;
    private EditText mPassword;
    private Button mButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_main);
        mUsername = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);
        mButton = findViewById(R.id.login);

        mButton.setOnClickListener(this);
    }

    @Override
    public void onShowMessage(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.login) {
            loginPresenter.login(mUsername.getText().toString(), mPassword.getText().toString());
        }

    }
}