package com.ecactus.androidappframework.mvvm.viewmodel;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ecactus.androidappframework.databinding.ActivityMvvmmainBinding;
import com.ecactus.androidappframework.mvvm.model.User;


    public class ViewModel {
        private ActivityMvvmmainBinding binding;
        public User user;
        public ViewModel(ActivityMvvmmainBinding binding,User user){
            this.binding = binding;
            this.user = user;
        }
        public void Login(View view){
            if (user.getmUserName().equals( "admin" ) && user.getmPassWord().equals( "12345" )){
                Log.d( "TAG","success" );
//                return true;
                Toast.makeText(binding.login.getContext(),"success",Toast.LENGTH_SHORT).show();
            }else {
                Log.d( "TAG","fail" );
//                return false;
                Toast.makeText(binding.login.getContext(),"fail",Toast.LENGTH_SHORT).show();
            }
        }
    }
