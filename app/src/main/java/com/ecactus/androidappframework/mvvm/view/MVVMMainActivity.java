package com.ecactus.androidappframework.mvvm.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.os.Bundle;
import android.widget.Toast;

import com.ecactus.androidappframework.R;
import com.ecactus.androidappframework.databinding.ActivityMvvmmainBinding;
import com.ecactus.androidappframework.mvvm.model.User;
import com.ecactus.androidappframework.mvvm.viewmodel.ViewModel;

public class MVVMMainActivity extends AppCompatActivity {
    private ActivityMvvmmainBinding binding;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_mvvmmain);
        user = new User("admin", "12345");
        ViewModel viewModel = new ViewModel(binding, user);
        binding.setViewmodel(viewModel);
    }
}