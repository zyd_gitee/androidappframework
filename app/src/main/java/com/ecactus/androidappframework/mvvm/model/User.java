package com.ecactus.androidappframework.mvvm.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;

import com.ecactus.androidappframework.BR;


public class User extends BaseObservable {
    public String mUserName;
    public String mPassWord;

    public User() {

    }

    public User(String mUserName, String mPassWord) {
        this.mUserName = mUserName;
        this.mPassWord = mPassWord;
    }

    @Bindable
    public String getmUserName() {
        return mUserName;
    }

    /**
     * @param mUserName
     */
    public void setmUserName(String mUserName) {
        this.mUserName = mUserName;
        notifyPropertyChanged(BR.mUserName);

    }

    @Bindable
    public String getmPassWord() {
        return mPassWord;
    }

    /**
     * @param mPassWord
     */
    public void setmPassWord(String mPassWord) {
        this.mPassWord = mPassWord;
        notifyPropertyChanged(BR.mPassWord);

    }
}

